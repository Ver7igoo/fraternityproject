<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Username : '
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name : '
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First name : '
            ])
            ->add('phone', TextType::class, [
                'label' => 'Phone number :'
            ])
            ->add('mail', TextType::class, [
                'label' => 'Mail : '
            ])
            ->add('password', PasswordType::class)
            ->add('admin')
            ->add('active')
            ->add('campus')//->add('trips')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
