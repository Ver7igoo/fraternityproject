<?php

namespace App\Form;

use App\Entity\Trip;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TripType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Your trip : '
            ])
            ->add('startDate', DateType::class, [
                'label' => 'Start date : '
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Duration of your trip : '
            ])
            ->add('closingDate', DateType::class, [
                'label' => 'Closing date : '
            ])
            ->add('numberMax', IntegerType::class, [
                'label' => 'Number of people max : '
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description of your trip : '
            ])
            ->add('tripState')
            ->add('urlPhoto')
            ->add('status')
            ->add('campus')
            ->add('location');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trip::class,
        ]);
    }
}
