<?php

namespace App\Controller;

use App\Entity\Trip;
use App\Form\TripType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class TripController extends AbstractController
{
    /**
     * @Route("/create_trip", name="create_trip")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create(EntityManagerInterface $em, Request $request)
    {
        $trip = new Trip();
        $tripForm = $this->createForm(TripType::class, $trip);
        $tripForm->handleRequest($request);
        if ($tripForm->isSubmitted() && $tripForm->isValid()) {
            $em->persist($trip);
            $em->flush();
        }

        return $this->render('trip/create.html.twig', [
            "tripForm" => $tripForm->createView(),
        ]);
    }

    /**
     * @Route("/trip", name="trip_list")
     */
    public function list(EntityManagerInterface $em, Request $request)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trips = $tripRepo->findAll();

        return $this->render('trip/list.html.twig', [
            //affichage des series
            "trips" => $trips
        ]);

    }

    /**
     * @Route("/trip/{id}", name="trip_detail")
     */
    public function detail($id, Request $request)
    {
        $tripRepo = $this->getDoctrine()->getRepository(Trip::class);
        $trip = $tripRepo->find($id);

        return $this->render('trip/detail.html.twig', ["trip" => $trip
        ]);
    }


}