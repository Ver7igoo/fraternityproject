<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/loginError", name="loginError")
     */
    public function index(): Response
    {
        return $this->render('user/loginError.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        // Symfony traite a notre place via Security.yaml
        return $this->render("user/login.html.twig");

    }

    /**
     * Symfony gere entierement cette route
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/update", name="update")
     */
    public function Update(Request $request, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $userForm = $this->createForm(UserType::class, $user);

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $em->persist($user);
            $em->flush();
        }
        return $this->render('user/update.html.twig', [
            "userForm" => $userForm->createView()
        ]);
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function Profile(EntityManagerInterface $em, Request $request)
    {
        $user = $this->getUser();

        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->find($user);

        return $this->render('user/profile.html.twig', ["user" => $user
        ]);
    }
}