<?php

namespace App\Entity;

use App\Repository\ConditionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConditionRepository::class)
 * @ORM\Table(name="`condition`")
 */
class Condition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $wording;

    /**
     * @ORM\OneToMany(targetEntity=Trip::class, mappedBy="status")
     */
    private $trips;

    public function __construct()
    {
        $this->trips = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getWording()
    {
        return $this->wording;
    }


    /**
     * @param mixed $wording
     */
    public function setWording($wording): void
    {
        $this->wording = $wording;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Trip[]
     */
    public function getTrips(): Collection
    {
        return $this->trips;
    }

    public function addTrip(Trip $trip): self
    {
        if (!$this->trips->contains($trip)) {
            $this->trips[] = $trip;
            $trip->setStatus($this);
        }

        return $this;
    }

    public function removeTrip(Trip $trip): self
    {
        if ($this->trips->removeElement($trip)) {
            // set the owning side to null (unless already changed)
            if ($trip->getStatus() === $this) {
                $trip->setStatus(null);
            }
        }

        return $this;
    }


}
